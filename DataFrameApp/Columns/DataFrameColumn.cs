﻿using System.Collections.Generic;

namespace DataFrameApp
{
    public class DataFrameColumn<T> : IDataFrameColumn
    {
        public string Name { get; private set; }
        public IEnumerable<T> Values { get; private set; }

        public DataFrameColumn(string name)
        {
            Name = name;
        }

        public DataFrameColumn(string name, IEnumerable<T> values)
            : this(name)
        {
            Values = values;
        }
    }

   
}

﻿using System;
using System.Collections.Generic;

namespace DataFrameApp
{
    public class IntDataFrameColumn : DataFrameColumn<int>, ICountUnique, ICalculateMean
    {
        public IntDataFrameColumn(string name)
            : base(name) { }

        public IntDataFrameColumn(string name, IEnumerable<int> values)
            : base(name, values) { }

        public int CountUnique()
        {
            throw new NotImplementedException();
        }

        public double Mean()
        {
            throw new NotImplementedException();
        }
    }

   
}

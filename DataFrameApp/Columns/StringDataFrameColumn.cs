﻿using System;
using System.Collections.Generic;

namespace DataFrameApp
{
    public class StringDataFrameColumn : DataFrameColumn<string>, ICountUnique
    {
        public StringDataFrameColumn(string name)
            : base(name) { }

        public StringDataFrameColumn(string name, IEnumerable<string> values)
            : base(name, values) { }

        public int CountUnique()
        {
            throw new NotImplementedException();
        }
    }

   
}

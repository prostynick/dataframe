﻿using System;
using System.Collections.Generic;

namespace DataFrameApp
{
    public class DataFrame
    {
        public IDataFrameColumn this[string columnName] { get => throw new NotImplementedException(); }
        public IEnumerable<IDataFrameColumn> Columns { get => throw new NotImplementedException(); }
        public IEnumerable<IDataFrameRow> Rows { get => throw new NotImplementedException(); }

        public DataFrame(IEnumerable<IDataFrameColumn> columns)
        {
            //store columns internally
        }

        public DataFrame OrderBy(string columnName) { throw new NotImplementedException(); }
        public IDataFrameGrouping<T> GroupBy<T>(string columnName) { throw new NotImplementedException(); }
        public IDataFrameGrouping<T> GroupBy<T>(string columnName, params string[] otherColumnsFilter) { throw new NotImplementedException(); }
    }

   
}

﻿namespace DataFrameApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Will obviously not run...
            //See README.md

            var c1 = new IntDataFrameColumn("Numbers", new[] { 4, 2, 5 });
            var c2 = new StringDataFrameColumn("Strings", new[] { "A", "B", "C" });
            var c3 = new DataFrameColumn<bool>("Booleans", new[] { true, false, true });

            var mean = c1.Mean();
            var uniq = c2.CountUnique();

            var df = new DataFrame(new IDataFrameColumn[] { c1, c2, c3 });

            df.OrderBy("Numbers");

            var grp = df.GroupBy<bool>("Booleans", "Numbers"); 
            grp.Count();
        }
    }

   
}

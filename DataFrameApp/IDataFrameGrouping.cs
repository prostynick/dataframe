﻿using System.Linq;
using System.Collections.Generic;

namespace DataFrameApp
{
    public interface IDataFrameGrouping<T>
    {
        IEnumerable<IGrouping<T, IDataFrameRow>> Groups { get; }

        DataFrame Count();
        DataFrame Average();
        DataFrame Sum();
    }

   
}

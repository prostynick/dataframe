﻿using System.IO;

namespace DataFrameApp
{
    public interface IDataFrameFactory
    {
        DataFrame CreateFromFile(string fileName);
        DataFrame CreateFromFile(StreamReader streamReader);
    }

   
}

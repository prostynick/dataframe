﻿using System.Collections.Generic;

namespace DataFrameApp
{
    public interface IDataFrameRow
    {
        IEnumerable<object> Items { get; }
    }

   
}
